#!/usr/bin/env python3

import argparse
import subprocess
import os
from os.path import join

MY_IOT_PATH = os.environ["MY_IOT_FRAMEWORK_PATH"]
MY_IOT_PROJECT = os.environ["MY_IOT_PROJECT"]

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run different docker-compose combo.')
    parser.add_argument('kind', choices=['dev', 'staging', 'prod'], help='a string for the release to run', default='prod')
    parser.add_argument('--comm',
                        dest='comm',
                        nargs='?',
                        action='store',
                        choices=['mqtt', 'ws', 'http'],
                        help='choose the communication channel')
    parser.add_argument('--monitor',
                        dest='monitoring',
                        action='store_const',
                        const='monitoring',
                        default=False,
                        help='enable prometheus monitoring stack')
    parser.add_argument('--final',
                        dest='final',
                        action='append',
                        default=[],
                        help='add one or more docker-compose.$final.yml (can be specified multiple times)')

    parser.add_argument('more_args', nargs='*', help='further args for docker-compose')

    args = parser.parse_args()

    args_docker_compose = ['docker-compose', '-p', MY_IOT_PROJECT]
    args_docker_compose += ['-f', 'docker-compose.yml']

    if os.path.isfile('docker-compose.{}.yml'.format(args.kind)):
        # Includes dev/staging/prod if docker-compose exists
        args_docker_compose += ['-f', join(MY_IOT_PATH, 'docker-compose.{}.yml'.format(args.kind))]

    if args.comm:
        # Includes specific communication channel as requested
        args_docker_compose += ['-f', join(MY_IOT_PATH, 'docker-compose.comm_{}.yml'.format(args.comm))]

    if args.monitoring:
        # Includes monitoring docker-compose if requested
        args_docker_compose += ['-f', join(MY_IOT_PATH, 'docker-compose.monitoring.yml')]

    for other in args.final:
        # Includes some other docker-compose files if requested
        # They can be multiple and usually they are spcific to project
        args_docker_compose += ['-f', other]

    print("Starting " + " ".join((args_docker_compose + args.more_args)) + "...")
    try:
        subprocess.run(args_docker_compose + args.more_args)
    except KeyboardInterrupt:
        print("Containers stopped")
