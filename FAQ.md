# FAQ

## Popolare il database con alcune schede di test

   `./manage_dev.sh exec web ./manage.py add_test_boards -c IT 10`

Il numero può variare e con l'opzione "-c" si può indicare una geolocalizzazione approssimata delle schede di test

Per utilizzare client di test seguire la guida in `TESTING.md`

## Caricare export da un'altra installazione

    `./manage_dev.sh exec web ./load_fixtures.sh fixtures.json`

## Muovere un volume docker da un'altra installazione

1. Fermare la piattaforma in esecuzione: `./manage_dev.sh stop`
2. Esportare il volume nel file backup.tar.gz: 

`docker run --rm --volumes-from <project name>_db_1 -v $(pwd):/backup busybox tar cvfz /backup/backup.tar.gz /var/lib/postgresql/data`

3. Trasferire il file `backup.tar.gz` generato nella directory corrente nel sistema di destinazione
4. Importare il volume dal file backup.tar.gz:

`docker run --rm --volumes-from <destination project name>_db_1 -v $(pwd):/backup busybox tar xvfz /backup/backup.tar.gz`


## Regenerate certs

In order to generate new certs we suggest using Let's Encrypt and `certbot` or `acme_install.sh` clients.

To generate testing self-signed certificates, here is the receipt:

```
openssl genrsa -out server.key 2048
openssl rsa -in server.key -out server.key
openssl req -sha256 -new -key server.key -out server.csr -subj '/CN=localhost'
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt
```

kudos: [https://stackoverflow.com/a/31984753/720743](https://stackoverflow.com/a/31984753/720743)

## Regenerate Diffie-Hellman dhparam.pem

`openssl dhparam -out dhparam.pem 2048`

kudos: [https://www.ibm.com/docs/en/zvse/6.2?topic=SSB27H_6.2.0/fa2ti_openssl_generate_dh_parms.html](https://www.ibm.com/docs/en/zvse/6.2?topic=SSB27H_6.2.0/fa2ti_openssl_generate_dh_parms.html)
