#!/bin/bash

source .env
PARAMS=""
MY_IOT_COMM=${MY_IOT_COMM:-http}

if [ -e "docker-compose.final.yml" ]; then
    PARAMS="--final=docker-compose.final.yml"
fi

export MY_IOT_PROJECT MY_IOT_FRAMEWORK_PATH
./compose.py --comm=${MY_IOT_COMM} ${PARAMS} -- dev stop
./compose.py --comm=${MY_IOT_COMM} ${PARAMS} -- dev up $@
