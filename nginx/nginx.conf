user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

  server {
    listen 81;

    location = /nginx_status {
      stub_status;
    }
  }


  server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name localhost;

    return 302 https://$server_name$request_uri;
  }

  server {
    listen 443 ssl;
    listen [::]:443 ssl;

    server_name localhost;
    ssl_certificate /etc/nginx/cert.pem;
    ssl_certificate_key /etc/nginx/privkey.pem;

    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/dhparam.pem;
    ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
    ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
    ssl_session_timeout  10m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off; # Requires nginx >= 1.5.9
    # DISABLED for self-signed cert ssl_stapling on; # Requires nginx >= 1.3.7
    # DISABLED for self-signed cert ssl_stapling_verify on; # Requires nginx => 1.3.7
    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;
    # Disable strict transport security for now. You can uncomment the following
    # line if you understand the implications.
    # add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";

    location = /grafana {
      return 302 $scheme://$http_host/grafana/;
    }

    location /grafana/ {
      # https://grafana.com/docs/grafana/latest/auth/auth-proxy/
      # https://github.com/grafana/grafana/issues/3752#issuecomment-485976560
      # http://nginx.org/en/docs/http/ngx_http_auth_request_module.html

      auth_request /authproxy/;
      auth_request_set $user $upstream_http_user;

      proxy_set_header X-WEBAUTH-USER $user;
      proxy_pass http://grafana:3000/;
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      # Remove the Authorization header
      proxy_set_header Authorization "";
    }

    location /static/ {
        alias /app/STATIC/;
    } 

    location /media/ {
        alias /app/MEDIA/;
    } 

    location / {
      proxy_pass http://web:8001;
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_redirect  off;
      access_log off;

    }
  }
}
